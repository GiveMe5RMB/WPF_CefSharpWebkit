﻿using CefWebkit.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CefWebkit
{
    /// <summary>
    /// InputPassword.xaml 的交互逻辑
    /// </summary>
    public partial class InputPassword : Window
    {
        private bool isPass = false;
        public bool IsPass { get => isPass; }

        private PassTypeEnum ePassType = PassTypeEnum.UnLock;//密码类型 
        public PassTypeEnum EPassType { get => ePassType; }

        public InputPassword()
        {
            InitializeComponent();
            this.Topmost = true;
        }

        /// <summary>
        /// 初始化窗体，带解锁类型
        /// </summary>
        /// <param name="passTypeEnum"></param>
        public InputPassword(PassTypeEnum passTypeEnum)
        {
            InitializeComponent();
            ePassType = passTypeEnum;
            this.Topmost = true;
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            isPass = false;
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            string strInputPassWord = txtPassWord.Password;

            if (string.IsNullOrEmpty(strInputPassWord))
            {
                MessageBox.Show(this,"请输入密码！","提示",MessageBoxButton.OK,MessageBoxImage.Asterisk);
                return;
            }

            string strPassWord = string.Empty;

            if (ePassType.Equals(PassTypeEnum.UnLock))
            {
                //退出解锁密码
                strPassWord = CryptographicHelper.Md532(DateTime.Now.ToString("yyyyMMdd") + (Convert.ToInt32(DateTime.Now.DayOfWeek) + 1).ToString()).Substring(0, 6);
            }
            else if (ePassType.Equals(PassTypeEnum.Admin))
            {
                //管理员密码
                strPassWord = CryptographicHelper.Md532(DateTime.Now.ToString("yyyyMMdd") + (Convert.ToInt32(DateTime.Now.DayOfWeek) + 1).ToString()).Substring(0, 8);
            }

            if (strInputPassWord.Equals(strPassWord))
            {
                isPass = true;
            }
            else
            {
                MessageBox.Show(this,"密码错误！", "提示", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }
            this.Close();
        }

        private void txtPassWord_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if(txtPassWord.Password.Length > 0)
            {
                btnOk.IsEnabled = true;
            }
            else
            {
                btnOk.IsEnabled = false;
            }
        }
    }
}
